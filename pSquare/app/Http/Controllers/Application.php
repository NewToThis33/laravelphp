<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Videos;

class Application extends Controller
{
  public function index() {

    $categories = Categories::all();
    $videos = Videos::all();

    return view('layouts.index', compact('categories', 'videos'));

  }
}
