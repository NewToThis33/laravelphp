<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Categories;

class Videos extends Controller
{
    public function index() {

      $categories = Categories::all();
      $videos = App\Videos::all();

      return view('layouts.videos', compact('categories', 'videos'));
    }

    public function show($id) {
      $categories = Categories::all();
      $video = App\Videos::find($id);
      $videos = App\Videos::All();

      return view('layouts.videoPage', compact('categories', 'video', 'videos', 'title', 'name', 'ref'));
    }
}
