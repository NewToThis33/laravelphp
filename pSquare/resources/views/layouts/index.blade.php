@extends('master')
@section('banner')
  <header class="col-xs-12 ">
      <img width="100%" src="{{asset('image\Banner.jpg')}}" alt="Рекламный банер">
  </header>
@endsection

@section('header')
  <nav id="navbar" class=" col-xs-12 navbar navbar-default" role="navigation">

      <div class="container">

          <div id="search" class="row">

              <div class="imageHolder col-xs-2">
                  <a href="index.html"><img src="{{asset('image\Logo.jpg')}}" alt="Логотип"></img>
                  </a>

              </div>

              <div id="Form" class=" col-xs-5">
                  <form class="navbar-form " role="search">

                      <div class="input-group">
                          <input type="text" class="form-control" placeholder="Найдите материалы, курсы и людей...">
                          <span class="input-group-btn">
                              <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> </button>
                          </span>
                          </span>

                      </div>
                  </form>

              </div>

              <div class="col-xs-2">

                  <div class="social row">

                      <div class="col-xs-4">
                          <script type="text/javascript">
                              document.write(VK.Share.button(false, {
                                  type: "custom",
                                  text: "<img src=\"https://vk.com/images/share_32.png\" width=\"32\" height=\"32\" />"
                              }));
                          </script>

                      </div>

                      <div class="col-xs-4">
                          <a href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a>

                      </div>

                      <div class="col-xs-4">
                          <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>

                      </div>

                  </div>

              </div>

              <div id="Registration" class=" col-xs-3 ">

                  <div class="btn-group pull-right">

                      <button class="btn btn-default">Регистрация</button>


                      <button class="btn btn-default">Войти</button>

                  </div>

              </div>

          </div>

      </div>
  </nav>
@endsection


@section('navbar')
  <div class="navbarMain col-xs-2 ">
      <ul class="nav nav-stacked">
        @foreach ($categories as $category)
          @if ($category->count_videos != 0 && $category->count_videos != NULL)
            @if ($category->id == 5 || $category->id == 19 || $category->id == 25 || $category->id == 28)

              @if ($category->id == 25 || $category->id == 28)

                @if ($category->id == 28)

                  <li><a href="{{ $category->id }}">{{$category->name}}&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></span></a></li>
                  <li class="divider"></li>

                @else

                  <li><a href="{{ $category->id }}">{{$category->name}}</a></li>
                  <li class="margin-bottom-08 divider"></li>

                @endif

              @else

                <li><a href="{{ $category->id }}">{{$category->name}}</a></li>
                <li class="divider"></li>

              @endif

            @else

              @if ($category->id == 6 || $category->id == 20)

                <li class="knoledgeBase">$category->name</li>

              @else

                @if ($category->id >= 26 && $category->id <= 27)

                  <li><a href="{{ $category->id }}">{{$category->name}}&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></span></a></li>

                @else

                  <li><a href="{{ $category->id }}">{{$category->name}}</a></li>

                @endif

              @endif

            @endif

          @else
            @if ($category->id == 5 || $category->id == 19 || $category->id == 25 || $category->id == 28)

              @if ($category->id == 25 || $category->id == 28)

                @if ($category->id == 28)

                  <li><a href="#">{{$category->name}}&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></span></a></li>
                  <li class="divider"></li>

                @else

                  <li><a href="#">{{$category->name}}</a></li>
                  <li class="margin-bottom-08 divider"></li>

                @endif

              @else

                <li><a href="#">{{$category->name}}</a></li>
                <li class="divider"></li>

              @endif

            @else

              @if ($category->id == 6 || $category->id == 20)

                <li class="knoledgeBase">{{$category->name}}</li>

              @else

                @if ($category->id >= 26 && $category->id <= 27)

                  <li><a href="#">{{$category->name}}&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></span></a></li>

                @else

                  <li><a href="#">{{$category->name}}</a></li>

                @endif

              @endif

            @endif

          @endif


        @endforeach

      </ul>

  </div>
@endsection

@section('content')
  <div class="col-xs-10">

      <div id="firstBlock" class=" firstBlock row">

          <div class="col-xs-6">

              <div class="thumbnail">
                  <a href="/video/{{$videos->first()->id}}"><img width="100%" src="https://img.youtube.com/vi/{{$videos->first()->ref}}/maxresdefault.jpg" alt=""></a>

              </div>

          </div>

          <div class="col-xs-6">

              <div class="thumbnail">
                  <!-- <div class="caption"> -->
                  <p class="link">СКОРО&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></p>
                  <h4 class="bold">25 декабря</h4>
                  <p>lorem ipsum dolor sit lorem ipsum lorem ipsum dolor sit lorem ipsum</p>
                  <h4 class="bold">27 декабря</h4>
                  <p>lorem ipsum dolor sit lorem ipsum lorem ipsum dolor sit lorem ipsum</p>
                  <h4 class="bold">14 января</h4>
                  <p>lorem ipsum dolor sit lorem ipsum lorem ipsum dolor sit lorem ipsum</p>
                  <!-- </div> -->

              </div>

          </div>

      </div>

      <div class="block secondBlock row">
        @foreach ($videos as $video)
          @if ($loop->index == 1)
            <div class="col-xs-4">
              <div class="thumbnail">
                  <a href="/video/{{$video->id}}"><img src="https://img.youtube.com/vi/{{$video->ref}}/maxresdefault.jpg" alt=""></a>
                  <p class="atribute"><span class="text-muted">{{$video->name}}</span></p>
                  <h4>{{$video->title}}</h4>

                  <span> 7 дней назад <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

              </div>
            </div>
          @endif
        @endforeach
        @foreach ($videos as $video)
          @if ($loop->index == 2)
            <div class="col-xs-8">

                <div class=" thumbnail">
                    <a href="/video/{{$video->id}}"><img width="100%" class="small" src="https://img.youtube.com/vi/{{$video->ref}}/maxresdefault.jpg" alt=""></a>

                </div>

            </div>
          @endif
        @endforeach

      </div>

      <div class="block thirdBlock row">

        @foreach ($videos as $video)
          @if ($loop->index == 3)
            <div class="col-xs-4">
              <div class="thumbnail">
                  <a href="/video/{{$video->id}}"><img src="https://img.youtube.com/vi/{{$video->ref}}/maxresdefault.jpg" alt=""></a>
                  <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                  <h4>{{$video->title}}</h4>

                  <span> 7 дней назад <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

              </div>
            </div>
          @endif
        @endforeach
        @foreach ($videos as $video)
          @if ($loop->index == 4)
            <div class="col-xs-8">

                <div class=" thumbnail">
                    <a href="/video/{{$video->id}}"><img width="100%" class="small" src="https://img.youtube.com/vi/{{$video->ref}}/maxresdefault.jpg" alt=""></a>

                </div>

            </div>
          @endif
        @endforeach

      </div>

      <div class="block row">
        @foreach ($videos as $video)
          @if ($loop->index >4 && $loop->index < 9 )
            <div class="col-xs-3">

                <div class="thumbnail">
                    <a href="/video/{{$video->id}}"><img src="https://img.youtube.com/vi/{{$video->ref}}/maxresdefault.jpg" alt=""></a>
                    <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                    <h4>{{$video->title}}</h4>

                    <span> 7 дней назад <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

                </div>

            </div>
          @endif
        @endforeach

      </div>

      <div class="block row ">

        @foreach ($videos as $video)
          @if ($loop->index >9 && $loop->index < 14)
            <div class="col-xs-3">

                <div class="thumbnail">
                    <a href="/video/{{$video->id}}"><img src="https://img.youtube.com/vi/{{$video->ref}}/maxresdefault.jpg" alt=""></a>
                    <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                    <h4>{{$video->title}}</h4>

                    <span> 7 дней назад <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

                </div>

            </div>
          @endif
        @endforeach
      </div>

      <div class="space row">

          <div class="padding-right-45 divide col-xs-5">
              <li class="divider"></li>

          </div>

          <div class="text col-xs-2">

              <p>ВЕБИНАРЫ</p>

          </div>

          <div class="padding-left-45 divide col-xs-5">
              <li class="divider"></li>

          </div>

      </div>

      <div class="block row">
        @php
          $count = 0;
        @endphp
        @foreach ($videos as $video)
          @if ($video->name == 'Вебинары' && $count < 4)

              <div class="col-xs-3">

                  <div class="thumbnail">
                      <a href="/video/{{$video->id}}"><img src="https://img.youtube.com/vi/{{$video->ref}}/maxresdefault.jpg" alt=""></a>
                      <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                      <h4>{{$video->title}}</h4>

                      <span> 7 дней назад <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

                  </div>
  </div>
@php
  $count++;
@endphp

          @endif

        @endforeach

          {{-- <div class="col-xs-3">

              <div class="thumbnail">
                  <a href="#"><img src="https://img.youtube.com/vi/B0svxwHhpLQ/maxresdefault.jpg" alt=""></a>
                  <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                  <h4>SistemaRosDom.ru - сообщество разумных соседей!</h4>

                  <span> 7 дней назад  <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

              </div>

          </div>

          <div class="col-xs-3">

              <div class="thumbnail">
                  <a href="#"><img src="https://img.youtube.com/vi/B0svxwHhpLQ/maxresdefault.jpg" alt=""></a>
                  <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                  <h4>SistemaRosDom.ru - сообщество разумных соседей!</h4>

                  <span> 7 дней назад  <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

              </div>

          </div>

          <div class="col-xs-3">

              <div class="thumbnail">
                  <a href="#"><img src="https://img.youtube.com/vi/B0svxwHhpLQ/maxresdefault.jpg" alt=""></a>
                  <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                  <h4>SistemaRosDom.ru - сообщество разумных соседей!</h4>

                  <span> 7 дней назад  <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

              </div>

          </div>

          <div class="col-xs-3">

              <div class="thumbnail">
                  <a href="#"><img src="https://img.youtube.com/vi/B0svxwHhpLQ/maxresdefault.jpg" alt=""></a>
                  <p class="atribute"><span class="text-muted">ВЕБИНАРЫ</span></p>
                  <h4>SistemaRosDom.ru - сообщество разумных соседей!</h4>
                  <span> 7 дней назад  <span class="glyphicon glyphicon-heart"></span> 35 <span class="glyphicon glyphicon-eye-open"></span> 10</span>

              </div>

          </div> --}}

      </div>

      <div class="preFooter row">

          <div class="col-xs-6">
              <p><span class="gray"><strong>Ц</strong>ентр <strong>Р</strong>азвития <strong>Ч</strong>еловеческого <strong>П</strong>отенциала</span> — сообщество мастеров имеющих четкую подтвержденную компетенцию в деле которому служат. Профессионалов
                  труда ставящих и достигающих личной цели. Людей желающих развиваться индивидуально и раньше других. Применяющих инструменты в работе раньше конкурентов.</p>

          </div>

          <div class="col-xs-5 col-xs-offset-1">
              <p class="gray">Система школ опытных преподавателей</p>

          </div>

      </div>

  </div>
@endsection

@section('footer')
  <footer class="navigation col-xs-12">

      <div class="margin-left-191 col-xs-12">
          <ul class="nav nav-pills">
              <li><a href="#">Партнерам</a></li>
              <li><a href="#">Контакты</a></li>
              <li><a href="#">Продукты ЛК</a></li>
              <li><a href="#">Контакты</a></li>
              <li><a href="#">Контакты</a></li>
              <li><a href="#">Контакты</a></li>
          </ul>

      </div>

      <div class="margin-left-166 col-xs-12">

          <div class="row">

              <div class="call col-xs-2">

                  <p>8 800 000 00 00</p>
                  <p>+7 900 000 00 00</p>
                  <button type="button" class="btn btn-primary">Заказать звонок</button>

              </div>

              <div class="location col-xs-10">
                  <p>Личный квадрат. ООО ЛК Общество с ограниченной ответственностью Личный квадрат</p>
                  <p>Юридический адрес: 600000, г.Владимир, ул.Девическая, д.8</p>
                  <p>ИНН 3376236787 КПП 339101001 ОГРН 1774789086432345</p>
                  &#169 2017

              </div>

          </div>

      </div>
  </footer>
@endsection
