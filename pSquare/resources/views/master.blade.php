<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

    <head>
        <meta charset="UTF-8">
        <meta name="yandex-verification" content="65c1bde065a59098" />
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('css\bootstrap-3.3.7-dist\js\bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="https://vk.com/js/api/share.js?94" charset="windows-1251"></script>
        <script type="text/javascript" src="{{ asset('js/Application.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/square.css')}}">
    </head>

    <body>
      <div class="ad container">

        <div class="row">
          @yield('banner')
        </div>

      </div>

      <div class="container">
        <div class="row">
          @yield('header')

          @yield('navbar')

          @yield('content')

          @yield('sidebar')
        </div>
        @yield('prefooter')
      </div>

    <div class="container">
        <div class="row">
        @yield('footer')
        </div>
    </div>
    </body>

</html>
